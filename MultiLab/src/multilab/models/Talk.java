package multilab.models;

public class Talk {
    
    private Person author;
    private Theme  theme;
    private String title;

    public Person getAuthor() { return author; }
    public String getTitle() { return title; }
    
    public void setAuthor(Person author) { this.author = author; }
    public void setTitle(String title) 
    {
        this.title = title;
    }

}