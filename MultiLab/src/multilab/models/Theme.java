package multilab.models;

public class Theme {
    
    private String name;
    
    public Theme(String name) throws Exception
    {
        this.setName(name);
    }
    
    private void setName(String name) throws Exception
    {
        if(name.isEmpty())
            throw new Exception("Le nom est incorrect !");
        this.name = name;
    }
    
    public String getName(){ return this.name; }
    
}