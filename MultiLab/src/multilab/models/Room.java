package multilab.models;

public class Room {

    private int seats;
    private String name;

    public int getSeats() { return seats; }
    public String getName() { return name; }
    public void setSeats(int seats) { this.seats = seats; }
    public void setName(String name) { this.name = name; }
    
}