package multilab;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Objects;
import java.util.Set;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * Created by Thimo on 7/03/2016.
 */
public class JsonParser {

    //variables
    String  filePath;

    public JsonParser(String filePath){
        this.filePath = filePath;
    }


    //methods
    public void doParse(){
        System.out.println("working");

        try{
            FileReader reader = new FileReader(filePath);
            JSONParser parser = new JSONParser();

            //retrieve array of objects
            JSONArray fullArray = (JSONArray) parser.parse(reader);

            //loop over all objects
            for (int i = 0; i < fullArray.size(); i++) {

                //one object
                JSONObject userObject = (JSONObject)fullArray.get(i);
                JSONObject userFields = (JSONObject)userObject.get("fields");

                //object's keyset
                Set objectKeys = userObject.keySet();
                Set userfieldKeys = userFields.keySet();

                //loop over first keyset
                for (Object objectKey : objectKeys) {
                    //value of key
                    Object val = userObject.get(objectKey.toString());
                    String className = val.getClass().getName();

                    //if value is another keyset, don't print
                    if(Objects.equals(className, "org.json.simple.JSONObject"))
                    {
                        //no print
                    }
                    else{
                        System.out.println(objectKey.toString() + " : " + userObject.get(objectKey.toString()));
                    }
                }

                //loop over second keyset
                for (Object userfieldKey : userfieldKeys) {
                    System.out.println(userfieldKey.toString() + " : " + userFields.get(userfieldKey.toString()));
                }



                System.out.println("\r\n -------- \r\n");
            }






        }
        catch (ParseException | IOException e) {
            e.printStackTrace();
        }
    }


}